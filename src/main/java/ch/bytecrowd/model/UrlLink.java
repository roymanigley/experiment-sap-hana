package ch.bytecrowd.model;

import java.util.Base64;
import java.util.Base64.Encoder;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class UrlLink {    
    
    private static final Encoder B64_ENCODER = Base64.getEncoder();

    public static UrlLink of(String url) {
        final UrlLink urlLink = new UrlLink();
        urlLink.setUrl(url);
        urlLink.setShorted(
            B64_ENCODER.encodeToString(url.getBytes())
        );
        return urlLink;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(nullable = false, unique = true)
    private String url;
    
    @Column(nullable = false, unique = true)
    private String shorted;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getShorted() {
        return shorted;
    }

    public void setShorted(String shorted) {
        this.shorted = shorted;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        UrlLink other = (UrlLink) obj;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "UrlLink [url=" + url + ", shorted=" + shorted + "]";
    }
}