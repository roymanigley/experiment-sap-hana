package ch.bytecrowd.clients;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class HanaJdbcClient {

    public final String connectionString;
	public final String user;
	public final String password;

	public HanaJdbcClient(String connectionString, String user, String password) {
		this.connectionString = connectionString;
		this.user = user;
		this.password = password;
	}
	
	public void executeQuery(String query) {
		try (Connection connection = DriverManager.getConnection(connectionString, user, password);) {
			
			System.out.println("[+] Connection to HANA successful!");
			
			try (Statement stmt = connection.createStatement();
			ResultSet resultSet = stmt.executeQuery(query);) {

				int columnCount = resultSet.getMetaData().getColumnCount();
				
				while (resultSet.next()) {
					System.out.println("[+] Begin Row-Nr: " + resultSet.getRow());
					for (int i = 1; i <= columnCount; i++) {
						System.out.println("    => column["+ i +"]: " + resultSet.getString(i));
					}
					System.out.println("[+] End RowNr: " + resultSet.getRow());
				}
				
			} catch (SQLException e) {
				System.err.println("[!] Query failed " + e.getMessage());
			}			
			
		} catch (SQLException e) {
			System.err.println("[!] Connection Failed. User/Passwd Error? Message: " + e.getMessage());
			e.printStackTrace();
		}
	}
}