package ch.bytecrowd.clients;

import ch.bytecrowd.model.UrlLink;
import ch.bytecrowd.repository.UrlLinkRepository;

public class HanaJpaClient {
    
    private final UrlLinkRepository repo;
    
    public HanaJpaClient(UrlLinkRepository repo) {
        this.repo = repo;
    }    
    
    public void create(String url) {
        System.out.println("[+] creating a new UrlLink for URL: " + url);
        UrlLink link = UrlLink.of(url);
        try {
            link = repo.save(link);
            System.out.println("[+] new UrlLink saved: " + link);            
        } catch (Exception e) {
            System.err.println("[!] saveing new UrlLink failed: " + link + ": " + e.getMessage()); 
        }
    }
    
    public void findAll() {
        System.out.println("[+] finding all UrlLinks");   
        repo.findAll().forEach(link -> System.out.println("[+] " + link));
    }
    
    public void deleteAll() {        
        System.out.println("[+] deleting all UrlLinks");
        try {            
            repo.deleteAll();   
            System.out.println("[+] all UrlLinks are deleted");
        } catch (Exception e) {
            System.err.println("[!] deleting all UrlLinks failed: " + e.getMessage()); 
        }
    }
}