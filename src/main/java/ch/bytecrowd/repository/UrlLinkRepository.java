package ch.bytecrowd.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import ch.bytecrowd.model.UrlLink;

@Repository
public interface UrlLinkRepository extends JpaRepository<UrlLink, Long> {
    
}