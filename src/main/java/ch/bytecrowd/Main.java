package ch.bytecrowd;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

import ch.bytecrowd.clients.HanaJdbcClient;
import ch.bytecrowd.clients.HanaJpaClient;
import ch.bytecrowd.repository.UrlLinkRepository;

@SpringBootApplication
public class Main {

	public static String connectionString = "jdbc:sap://192.168.0.100:39017;failoverserver1:39017;failoverserver2:39017";
	public static String user = "SYSTEM";
	public static String password = "HXEHana1";

	public static void main(String[] args) {

		// JDBC
		final HanaJdbcClient jdbcClient = new HanaJdbcClient(connectionString, user, password);
		jdbcClient.executeQuery("select 'hello', 'world', '!' from dummy");

		
		// JPA
		ConfigurableApplicationContext ctx = SpringApplication.run(Main.class, args);
		final UrlLinkRepository repo = ctx.getBean(UrlLinkRepository.class);

		final HanaJpaClient jpaClient = new HanaJpaClient(repo);
		jpaClient.create("https://google.com");
		jpaClient.create("https://github.com");
		jpaClient.create("https://example.com");
		jpaClient.findAll();
		jpaClient.deleteAll();
	}

}
