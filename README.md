# experiment-sap-hanadb
> the goal is to connect and to a SAP HANA DB, the DB was installed regarding this [guide](https://developers.sap.com/tutorials/hxe-ua-install-using-docker.html) on a Linux Debian 8GB RAM  

**Following experiments were made**  
- Old shool JDBC `ch.bytecrowd.clients.HanaJdbcClient`  
- Spring Boot with JPARepositories `ch.bytecrowd.clients.HanaJpaClient`  

## Dependencies
- Apache Maven
- Java 1.8

## Build
```
mvn clean package
```
## Run
```
mvn clean spring-boot:run
```

## Sources

- [Install SAL HANA XE in Docker](https://developers.sap.com/tutorials/hxe-ua-install-using-docker.html)
- [Connect to a SAP HANA XE using JDBC](https://developers.sap.com/tutorials/hxe-connect-hxe-using-jdbc.html)